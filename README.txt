--------------------------------------------------------------------------------
The Drug-drug Interaction and Drug-drug Interaction Evidence Ontology (DIDEO)
--------------------------------------------------------------------------------


A new foundational representation of potential drug-drug interactions
(PDDIs) that covers the material entities and processes in the domain
of discourse for PDDI evidence and knowledge claims. The
representation will enable the integration of drug interaction
mechanisms, effects, risk factors, severity, and management options
with the chemical and pharmacological properties (e.g., chemical
structure, function, pharmacokinetic and pharmacodynamic properties)
of the interacting drugs.

A description of the goals of the project can be found in the
following workshop paper located in the docs/DIDEO-discussion-ICBO-Brochhausen-2014.pdf:

Brochhausen, M., Schneider, J., Malone, D., Empey, PE., Hogan WR., and Boyce, RD. Towards a foundational representation of potential drug-drug interaction knowledge. The 1st International Drug-Drug Interaction Knowledge Representation Workshop (DIKR 2014). Collocated with the 2014 International Conference on Biomedical Ontology (ICBO 2014). October 6th, Houston, Texas. United States.


------------------------------------------------------------
Ontology development guidelines: 
------------------------------------------------------------

TODO - add to repository


------------------------------------------------------------
License
------------------------------------------------------------=

TODO: add license

------------------------------------------------------------
Contributors
------------------------------------------------------------

Richard D. Boyce
Mathias Brochhausen
Philip E. Empey
William R. Hogan
Daniel C. Malone
Jodi Schneider